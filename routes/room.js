var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	db.getConnection(function(err, con) {
		con.query("select * from room", function(e, r, f) {
			res.json({result: r});
			con.release();
		})
	});
})

router.get('/:rId', function(req, res) {
	db.getConnection(function(err, con) {
		con.query("select * from room where rId="+ req.params.rId, function(e, r, f) {
			res.json({result: r});
			con.release();
		});
	});
})

router.post('/save', function(req, res) {
	var room = req.body.room;
	db.getConnection(function(err, con) {
		if(room.hasOwnProperty('rId') && room.rId !== '') {
			con.query("UPDATE `room` SET `title` = ?, `description` = ? WHERE `rId` = ?",
				[room.title,
				room.description,
				room.rId], function(e, r, f) {
					res.json({result: r});
					con.release();
				});
		} else {
			con.query("INSERT INTO `room` SET ?", room, function(e, r, f) {
				res.json({result: (r.affectedRows == 1)});
				con.release();
			})
		}
	})
})

router.post('/delete', function(req, res) {
	if(req.body.rId === '' || req.body.rId === undefined) {
		res.json({result: "no id"});
	} else {
		db.getConnection(function(err, con) {
			con.query("DELETE FROM `room` WHERE `rId` = "+ req.body.rId, function(e, r, f) {
				res.json({result: e ? e : r});
				con.release();
			})
		})
	}

})

module.exports = router;