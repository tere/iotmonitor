var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	db.getConnection(function(err, con) {
		con.query("select * from sensor", function(e, r, f) {
			res.json({result: r});
			con.release();
		})
	});
})

router.get('/:sId', function(req, res) {
	db.getConnection(function(err, con) {
		con.query("select * from sensor where sId="+ req.params.sId, function(e, r, f) {
			res.json({result: r});
			con.release();
		});
	});
})

router.post('/save', function(req, res) {
	var sensor = req.body.sensor;
	db.getConnection(function(err, con) {
		if(sensor.hasOwnProperty('sId') && room.rId !== '') {
			con.query("UPDATE `sensor` SET `type` = ?, `rId` = ? WHERE `sId` = ?",
				[sensor.type,
				sensor.rId,
				sensor.sId], function(e, r, f) {
					res.json({result: r});
					con.release();
				});
		} else {
			con.query("INSERT INTO `sensor` SET ?", sensor, function(e, r, f) {
				console.log(r);
				res.json({result: e ? e : (r.affectedRows == 1)});
				con.release();
			})
		}
	})
})

router.post('/delete', function(req, res) {
	if(req.body.sId === '' || req.body.sId === undefined) {
		res.json({result: "no id"});
	} else {
		db.getConnection(function(err, con) {
			con.query("DELETE FROM `sensor` WHERE `sId` = "+ req.body.sId, function(e, r, f) {
				res.json({result: e ? e : r});
				con.release();
			})
		})
	}

})

module.exports = router;