var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	db.getConnection(function(err, con) {
		con.query("select * from log", function(e, r, f) {
			res.json({result: r});
			con.release();
		})
	});
})

router.get('/:lId', function(req, res) {
	db.getConnection(function(err, con) {
		con.query("select * from log where lId="+ req.params.lId, function(e, r, f) {
			res.json({result: r});
			con.release();
		});
	});
})

router.post('/save', function(req, res) {
	var log = req.body.log;
	db.getConnection(function(err, con) {
		con.query("INSERT INTO `log` SET ?", log, function(e, r, f) {
			res.json({result: e ? e : (r.affectedRows == 1)});
			con.release();
		})
	})
})

module.exports = router;