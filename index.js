var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var cors = require('cors');
var http = require('http');


var port = 3000;

global.db  = mysql.createPool({
  connectionLimit : 10,
  host            : 'localhost',
  user            : 'root',
  password        : '',
  database        : 'iotMonitor',
  charset:   'UTF8_ROMANIAN_CI'
});

var app = express();
app.use(cors())
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


var room = require('./routes/room');
var sensor = require('./routes/sensor');
var log = require('./routes/log');

app.use('/room', room);
app.use('/sensor', sensor);
app.use('/log', log);

var server = http.createServer(app);
server.listen(port, function() {
	console.log(`Listening on port ${port}`);
});